﻿using Atm.BLL.Interfaces;
using Atm.DATA.Domain;
using Atm.DATA.Enums;
using System;

namespace Atm.BLL.Implementation
{
    public class AtmService : IAtmService
    {
        private ATM _atm;
        private readonly  IAdminService _adminService;

        public AtmService(IAdminService adminService)
        {
            _adminService = adminService;
        }
        public void Start()
        {
            _atm = new ATM
            {
                Name = "Abeg ATM",
                AvailableCash = 50_000_000
            };

            Console.WriteLine("ATM has booted!");
            Console.WriteLine("Insert Card!");
        }

        public void Withdraw(decimal amount)
        {
            Console.WriteLine(amount);
        }

        public void CheckBalance(AccountType accountType)
        {
            Console.WriteLine(accountType);
        }

        public void Transfer(string accountNo, Bank bank, decimal amount)
        {
            Console.WriteLine(accountNo);
        }

        public void Deposit(string accountNo, AccountType accountType, decimal amount)
        {
            Console.WriteLine(accountNo);
        }

        public void PayBill(Bill bill)
        {
            Console.WriteLine(nameof(bill));
        }

        public void CreateAccount(string accountNo, AccountType accountType)
        {
            Console.WriteLine(nameof(CreateAccount));
        }

        public void ReloadCash(decimal amount)
        {
           _adminService.ReloadCash(amount, _atm);
        }
    }
}
