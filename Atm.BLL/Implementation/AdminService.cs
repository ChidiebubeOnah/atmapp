﻿using System;
using Atm.BLL.Interfaces;
using Atm.DATA.Domain;

namespace Atm.BLL.Implementation
{
    public class AdminService : IAdminService
    {
        public void ReloadCash(decimal amount, ATM atm)
        {
            Console.WriteLine("reloading {0}...", amount);

            atm.AvailableCash += amount;

            Console.WriteLine($"balance :: {atm.AvailableCash}");
        }
    }
}