﻿using System;
using Atm.BLL.Interfaces;

namespace Atm.BLL.Implementation
{
    internal class AuthService : IAuthService
    {
        public void Login(string userName, string password)
        {
            Console.WriteLine($"{userName} {password}");
        }

        public void ResetPin(string cardNumber, string accNo)
        {
            Console.WriteLine($"{cardNumber} {accNo}");
        }

        public void LogOut()
        {
            Console.WriteLine("Logging out....");
        }
    }
}