﻿using Atm.DATA.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atm.BLL.Interfaces
{
    public interface IAdminService
    {
        void ReloadCash(decimal amount, ATM atm);
    }
}
