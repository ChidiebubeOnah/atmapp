﻿
namespace Atm.BLL.Interfaces
{
    internal interface IAuthService
    {
        void Login(string userName, string password);

        void ResetPin(string cardNumber, string accNo);

        void LogOut();

    }
}
