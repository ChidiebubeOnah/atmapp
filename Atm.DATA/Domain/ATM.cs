﻿using Atm.DATA.Enums;

namespace Atm.DATA.Domain
{
    public class ATM
    {
        public string Name { get; set; }
        public decimal AvailableCash { get; set; }
        public Language CurrentLanguage { get; set; }
    }
}