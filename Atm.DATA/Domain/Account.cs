﻿using System;
using Atm.DATA.Enums;

namespace Atm.DATA.Domain
{
    public class Account
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string AccountNo { get; set; }
        public AccountType AccountType { get; set; }
        public string Pin { get; set; }
        public decimal Balance { get; set; }

    }
}