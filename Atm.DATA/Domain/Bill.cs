﻿namespace Atm.DATA.Domain
{
    public class Bill
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
}