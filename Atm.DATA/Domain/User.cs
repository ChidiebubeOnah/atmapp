﻿using System;
using System.Collections.Generic;
using System.Text;
using Atm.DATA.Enums;

namespace Atm.DATA.Domain
{
    internal abstract class User
    {

        protected User(long id, string password)
        {
            Password = password;
            Id = id;
        }

        protected User()
        {

        }

        public long Id { get; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; }
        public string PhoneNumber { get; set; }
        public virtual Role Role { get; }

    }
}
