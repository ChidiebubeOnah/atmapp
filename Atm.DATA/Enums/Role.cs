﻿namespace Atm.DATA.Enums
{
    internal enum Role
    {
        Customer,
        Admin,
        ThirdParty
    }
}