﻿namespace Atm.DATA.Enums
{
    
    public enum AccountType
    {
        Savings = 1,
        Current = 2
    }
}