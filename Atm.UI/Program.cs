﻿using Atm.BLL.Implementation;
using Atm.BLL.Interfaces;

namespace Atm.UI
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IAtmService atmService = new AtmService(new AdminService());
            atmService.Start();
            atmService.ReloadCash(122222);
            
        }
    }
}